{-# OPTIONS_GHC -Wno-orphans #-}

module Data.Extensible.IAlternativeState () where


import           Data.Extensible


instance IAlternativeState (Record xs) where
  ialternativeState :: a -> b
  ialternativeStateFold :: [a] -> b
  ialternativeStateFold1 :: NonEmpty a -> b
