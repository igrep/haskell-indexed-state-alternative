{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE TypeFamilies #-}

module Control.Monad.Indexed.State.Alternative
  ( IxApplicativeZero (..)
  , IxStateAlternative (..)
  , IAlternativeState (..)
  , (<<|>>)
  ) where

import           Control.Arrow (second)
import           Control.Applicative         (Alternative, empty, some, many, (<|>))
import           Control.Monad.Indexed       (ireturn)
import           Control.Monad.Indexed.State (IxMonadState, IxStateT (IxStateT),
                                              imodify)
import           Data.Functor.Indexed        (IxApplicative, (<<$>>), (<<*>>), (*>>))
import           Data.List.NonEmpty (NonEmpty, fromList)


class IAlternativeState a b | a -> b where
  ialternativeState :: a -> b
  ialternativeStateFold :: [a] -> b
  ialternativeStateFold1 :: NonEmpty a -> b

class IxApplicative m => IxApplicativeZero m where
  iempty :: m i j a

class (IxApplicativeZero m, IxMonadState m) => IxStateAlternative m where
  ialternative :: (IAlternativeState j l, IAlternativeState k l) => m i j a -> m i k a -> m i l a
  isome :: IAlternativeState j k => m i j a -> m i k [a]
  imany :: IAlternativeState j k => m i j a -> m i k [a]

instance (Monad m, Alternative m) => IxApplicativeZero (IxStateT m) where
  iempty = IxStateT $ const empty

instance (Monad m, Alternative m) => IxStateAlternative (IxStateT m) where
  ialternative (IxStateT m) (IxStateT n) =
    IxStateT $ \s ->
      (second ialternativeState <$> m s) <|> (second ialternativeState <$> n s)

  isome (IxStateT v) = IxStateT $ \s -> second (ialternativeStateFold1 . fromList) . unzip <$> some (v s)

  imany (IxStateT v) = IxStateT $ \s -> second ialternativeStateFold . unzip <$> many (v s)

infixl 3 <<|>>
{-# INLINE (<<|>>) #-}
(<<|>>) :: (IxStateAlternative m, IAlternativeState j l, IAlternativeState k l) => m i j a -> m i k a -> m i l a
(<<|>>) = ialternative
